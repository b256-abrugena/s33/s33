//1: Fetch Request using the GET method to retrieve all data form the JSON Placeholder API
async function fetchData() {
	let result = await fetch("https://jsonplaceholder.typicode.com/todos")
	let oldArr = await result.json()

	// 2. Creation of Array using Map Method to return only the title of every item 
	let newArr = oldArr.map(function(item){
		return item.title;
	})

	console.log(newArr);
}

fetchData();


//3. Fetch Request using the GET method that will retrieve a single to-do list item
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(response => response.json())
.then(json => console.log(json))


//4. Fetch Request using the POST method
fetch("https://jsonplaceholder.typicode.com/todos/", {
	method: "POST",
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		userId: 1,
		id: 201,
		title: "Created To Do List Item",
		completed: false
	})
})
.then(response => response.json())
.then(json => console.log(json))


//5. Fetch Request using the PUT method

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: "Updated To Do List Item",
		description: "To update the my to-do list with a different data structure",
		status: "Pending",
		dateCompleted: "Pending",
		userId: 1,
		id: 1
	})
})
.then(response => response.json())
.then(json => console.log(json))


//6. Fetch Request using the PATCH method

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		status: "Complete",
		dateCompleted: "07/09/21"
	})
})
.then(response => response.json())
.then(json => console.log(json))


//7. Fetch Request using the DELETE method
fetch("https://jsonplaceholder.typicode.com/todos/201", {
	method: "DELETE"
})

